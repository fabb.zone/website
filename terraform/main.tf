provider "vercel" {
  api_token = var.vercel_api_token
  team = "fabbzone"
}

resource "vercel_project" "website" {
  name      = "website"
  framework = "nuxtjs"
  vercel_authentication = {
    deployment_type = "none"
  }
  serverless_function_region = "fra1"

  git_repository = {
    type = "gitlab"
    repo = "fabb.zone/website"
  }
}

resource "vercel_project_domain" "www" {
  project_id = vercel_project.website.id
  domain     = "fabb.zone"
}

resource "vercel_project_domain" "backup" {
  project_id = vercel_project.website.id
  domain = "website-peach-seven.vercel.app"
}

