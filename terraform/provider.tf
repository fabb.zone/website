terraform {
  required_providers {
    vercel = {
      source  = "vercel/vercel"
      version = "~> 2.1.0"
    }
  }
  backend "http" {}
}

