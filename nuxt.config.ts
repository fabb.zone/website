const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname


export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@nuxt/content', '@nuxtjs/color-mode', "nuxt-icon"],
  target: 'static',
  router: {
    base
  },
  generate: {
    dir: 'public',
  },
  colorMode: {
    preference: 'light',
  },
})
