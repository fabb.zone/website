# fabb.zone

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

Pushing the code to some branch will create a review app with [Vercel](https://vercel.com).

## Production

The production server will be built automatically with [Vercel](https://vercel.com) and accessible through https://fabb.zone
once the pipeline for the main branch has passed.

## Contributing

Look for existing issues and submit a merge request!
