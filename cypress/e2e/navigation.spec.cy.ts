describe("Navigation", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  })
  it("visits the main page and sees greeting", () => {
    cy.contains("HELLO");
  });
  it("can go to the about page", () => {
    cy.contains("About").click();
    cy.url().should("include", "/about");
  });
  it("can go to the contact page", () => {
    cy.contains("Contact").click();
    cy.url().should("include", "/contact");
  });
  it("can return to the home page", () => {
    cy.visit("http://localhost:3000/about");
    cy.contains("fabb.zone").click();
    cy.url().should("not.include", "/about");
    cy.visit("http://localhost:3000/contact");
    cy.contains("fabb.zone").click();
    cy.url().should("not.include", "/contact");
  });
});
