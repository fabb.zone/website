describe("Theme switcher", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  })
  it("can change the theme", () => {
    const lightColor = "rgb(248, 248, 242)";
    const darkColor = "rgb(40, 42, 54)";
    cy.get("body").then(body => {
      const bgColor = body.css("background-color");
      const textColor = body.css("color");

      cy.get("button:has(.theme) svg").then(svg => {
        const iconType = svg.attr("id");
        if (iconType.includes("moon")) {
          cy.log("The icon is a moon.");
          expect(bgColor).eq(darkColor, "The background should be dark!")
          expect(textColor).eq(lightColor, "The foreground should be light!")
        } else if (iconType.includes("sun")) {
          cy.log("The icon is a sun.");
          expect(bgColor, "The ").eq(lightColor, "The background should be light!")
          expect(textColor).eq(darkColor, "The foreground should be dark!")
        } else {
          cy.log(`Unknown icon type: ${iconType}`);
        }
      });

      cy.get("button:has(.theme)").wait(3000).click();

      cy.get("body").then(body => {
        const newBgColor = body.css("background-color");
        const newTextColor = body.css("color");

        expect(newBgColor).not.to.eq(bgColor);
        expect(newTextColor).not.to.eq(textColor);

        cy.get("button:has(.theme) svg").then(svg => {
          const iconType = svg.attr("id");
          if (iconType.includes("moon")) {
            cy.log("The icon is a moon.");
            expect(newBgColor).eq(darkColor, "The background should be dark!")
            expect(newTextColor).eq(lightColor, "The foreground should be light!")
          } else if (iconType.includes("sun")) {
            cy.log("The icon is a sun.");
            expect(newBgColor, "The ").eq(lightColor, "The background should be light!")
            expect(newTextColor).eq(darkColor, "The foreground should be dark!")
          } else {
            cy.log(`Unknown icon type: ${iconType}`);
          }
        });
      });
    });
  });
});
